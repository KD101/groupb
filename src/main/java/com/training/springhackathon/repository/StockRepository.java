package com.training.springhackathon.repository;

import com.training.springhackathon.model.StockTrade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface StockRepository extends JpaRepository<StockTrade, Long> {

    //@QueryHints(value = { @QueryHint(name = "stockTicker"), value = })
    List<StockTrade> findByStockTicker(String stockTicker);




}
