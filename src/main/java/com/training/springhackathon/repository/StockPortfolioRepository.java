package com.training.springhackathon.repository;

import com.training.springhackathon.model.StockPortfolio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockPortfolioRepository extends JpaRepository<StockPortfolio, Long> {
    Optional<StockPortfolio> findByStockTicker(String stockTicker);
}
