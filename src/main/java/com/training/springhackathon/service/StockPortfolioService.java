package com.training.springhackathon.service;


import com.training.springhackathon.model.StockPortfolio;
import com.training.springhackathon.model.StockTrade;
import com.training.springhackathon.repository.StockPortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StockPortfolioService {

   @Autowired
   StockPortfolioRepository portfolioRepository;
   @Autowired
   private JdbcTemplate template;

   public List<StockPortfolio> findAll() {
      List<StockPortfolio> updatedPortfolios = new ArrayList<>();
      for (StockPortfolio portfolio: portfolioRepository.findAll()) {
         portfolio.setAvgPrice(calculateAvgPrice(portfolio.getStockTicker()));
         updatedPortfolios.add(portfolio);
      }
      return updatedPortfolios;
   }

   public StockPortfolio findById(Long id) {
      StockPortfolio portfolio = portfolioRepository.findById(id).get();

      portfolio.setAvgPrice(calculateAvgPrice(portfolio.getStockTicker()));
      return update(portfolio);
   }

   public StockPortfolio save(StockPortfolio portfolio) {
      return this.portfolioRepository.save(portfolio);
   }

   public StockPortfolio update(StockPortfolio portfolio) {
      if (portfolioRepository.findById(portfolio.getId()).isPresent())
         return portfolioRepository.save(portfolio);
      throw new NoSuchElementException();
   }

   /**
    * Increases holdings value of the Portfolio object of the trade.stockTicker when side is "buy",
    * decreases if side is sell.
    * A new Portfolio object will be posted to the database if one isn't present.
    *
    * @param trade that was last executed, volume is used to update portfolio holdings value
    * @return updated {@link StockPortfolio} object
    */
   public StockPortfolio updateHoldings(StockTrade trade) {
      StockPortfolio portfolio;
      // Create portfolio if not present
      if (portfolioRepository.findByStockTicker(trade.getStockTicker()).isEmpty())
         portfolio = save(new StockPortfolio(trade.getStockTicker()));
      else
         portfolio = portfolioRepository.findByStockTicker(trade.getStockTicker()).get();

      // Update holdings
      if (trade.getBuyOrSell().equalsIgnoreCase("buy"))
         portfolio.setHoldings(portfolio.getHoldings() + trade.getVolume());
      else if (trade.getBuyOrSell().equalsIgnoreCase("sell"))
         portfolio.setHoldings(portfolio.getHoldings() - trade.getVolume());

      return portfolioRepository.save(portfolio);

   }

   public void delete(Long id) {
      portfolioRepository.deleteById(id);
   }

   /**
    * Queries the database to return the average price of trades
    * @param stockTicker filters the query to a specific stockTicker
    * @return the average price, rounded to 2 decimal places
    */
   private Double calculateAvgPrice(String stockTicker) {
      return Math.round(template.queryForObject("select avg(price) from stocktrades where stockticker = '" + stockTicker +"'", Double.class) * 100.0) / 100.0;
   }
}
