package com.training.springhackathon.service;


import com.training.springhackathon.model.StockTrade;
import com.training.springhackathon.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * StockService class, worked on by @KurtisDickson
 */
@Service
public class StockService {

    @Autowired
    StockRepository stockRepository;
    @Autowired
    StockPortfolioService portfolioService;

    /**
     * Method to find all enteries in the MySQL database
     * @return JSON of MySQL table
     */
    public List<StockTrade> findAll() {
        return stockRepository.findAll();
    }

    /**
     * Method used to add stocktrades into the MySQL database.
     * @param stockTrade
     * @return
     */
    public StockTrade save(StockTrade stockTrade) {
        StockTrade trade = this.stockRepository.save(stockTrade);
        portfolioService.updateHoldings(trade);
        return trade;
    }

    /**
     * Method to update existing stock trades in the MySQL database.
     * @param stockTrade
     * @return
     */
    public StockTrade update(StockTrade stockTrade) {
        stockRepository.findById(stockTrade.getId()).get();

        return stockRepository.save(stockTrade);
    }

    /**
     * Method to delete stocktrades from the MySQL database.
     * @param id
     */
    public void delete(Long id) {
        stockRepository.deleteById(id);
    }

    /**
     * Method to find stocktrade by id from the MySQL database.
     * @param id
     * @return
     */
    public StockTrade findById(Long id) {

        return stockRepository.findById(id).get();
    }

    public List<StockTrade> findByStockTicker(String stockTicker) {
        return stockRepository.findByStockTicker(stockTicker);
    }


}
