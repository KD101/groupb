package com.training.springhackathon.model;

import javax.persistence.*;

@Entity(name="stockportfolio")
public class StockPortfolio {
    // Variables -------------------------------------------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "portfolioid")
    private Long id;
    @Column(name = "stockticker")
    private String stockTicker;
    private Integer holdings; // how much of the stock is owned
    @Column(name = "avgprice")
    private Double avgPrice; // the average price of all orders of this stock ticker
    private Double value; // market price * holdings

    // Getters & Setters -----------------------------------------------------------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public Integer getHoldings() {
        return holdings;
    }

    public void setHoldings(Integer holdings) {
        this.holdings = holdings;
    }

    public Double getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(Double avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    // Methods ---------------------------------------------------------------------------------------------------------
    public StockPortfolio() {}

    public StockPortfolio(String stockTicker) {
        this.stockTicker = stockTicker;
        this.holdings = 0;
        this.avgPrice = 0.0;
        this.value = 0.0;
    }

    @Override
    public String toString() {
        return "StockPortfolio{" +
                "stockTicker='" + stockTicker + '\'' +
                ", holdings=" + holdings +
                ", avgPrice=" + avgPrice +
                ", value=" + value +
                '}';
    }

}
