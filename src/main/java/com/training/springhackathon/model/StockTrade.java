package com.training.springhackathon.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Entity class was created by @JoshuaEntwistle
 */
@Entity(name="stocktrades")
public class StockTrade {
    // Variables -------------------------------------------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tradeid")
    private Long id;
    @Column(name = "stockticker")
    private String stockTicker;
    private Double price;
    private Integer volume;
    @Column(name = "buyorsell")
    private String buyOrSell;
    @Column(name = "statuscode")
    private Integer statusCode;
    @Column(name= "date")
    private String createdTimestamp = formatTimestamp(LocalDateTime.now());

    // Getters & Setters -----------------------------------------------------------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    // Methods ---------------------------------------------------------------------------------------------------------

    /**
     * Empty Constructor
     */
    public StockTrade() {}

    /**
     * Constructor with parameters.
     * @param id
     * @param stockTicker
     * @param price
     * @param volume
     * @param buyOrSell
     * @param statusCode
     */
    public StockTrade(Long id, String stockTicker, Double price, Integer volume, String buyOrSell, Integer statusCode) {
        this.id = id;
        this.stockTicker = stockTicker;
        this.price = price;
        this.volume = volume;
        this.buyOrSell = buyOrSell;
        this.statusCode = statusCode;
    }

    /**
     * To string constructor
     * @return
     */
    @Override
    public String toString() {
        return "StockTrade{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                ", createdTimestamp=" + createdTimestamp +
                '}';
    }

    private static String formatTimestamp(LocalDateTime datetime) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return datetime.format(formatter);
    }

}
