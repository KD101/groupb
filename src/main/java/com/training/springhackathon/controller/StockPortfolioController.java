package com.training.springhackathon.controller;

import com.training.springhackathon.model.StockPortfolio;
import com.training.springhackathon.model.StockTrade;
import com.training.springhackathon.service.StockPortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/portfolio")
public class StockPortfolioController {
    @Autowired
    private StockPortfolioService portfolioService;

    @GetMapping
    public List<StockPortfolio> findAll() {
        return this.portfolioService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<StockPortfolio> findById(@PathVariable Long id) {
        try {
            return new ResponseEntity<>(portfolioService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            //return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<StockPortfolio> save(@RequestBody StockPortfolio stockPortfolio) {
        return new ResponseEntity<>(portfolioService.save(stockPortfolio), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<StockPortfolio> update(@RequestBody StockPortfolio portfolio) {
        try {
            return new ResponseEntity<>(portfolioService.update(portfolio), HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/holdings")
    public ResponseEntity<StockPortfolio> updateHoldings(@RequestBody StockTrade trade) {
        try {
            return new ResponseEntity<>(portfolioService.updateHoldings(trade), HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        try {
            portfolioService.delete(id);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch(EmptyResultDataAccessException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

}
