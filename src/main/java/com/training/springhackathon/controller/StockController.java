package com.training.springhackathon.controller;

import com.training.springhackathon.model.StockTrade;
import com.training.springhackathon.service.StockService;
import org.springframework.web.bind.annotation.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Controller class created by @ChloeEdgar
 */
@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/stock")
public class StockController {

    private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StockService stockService;

    /**
     * Method for finding all entities in the table
     * @return
     */
    @GetMapping
    public List<StockTrade> findAll() {
        return this.stockService.findAll();
    }

    /**
     * Method for finding by id
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<StockTrade> findById(@PathVariable Long id) {
        try {
            return new ResponseEntity<StockTrade>(stockService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            //return 404
            LOG.debug("find for unknown id: [" + id + "]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Method for finding stock by ticker
     * @param stockTicker
     * @return
     */
    @GetMapping("/ticker/{stockTicker}")
    public ResponseEntity<List<StockTrade>> findByStockTicker(@PathVariable String stockTicker){
        try {
            return new ResponseEntity<List<StockTrade>>(stockService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            LOG.debug("find for unknown stock ticker: [" + stockTicker + "]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Method for saving stocktrade
     * @param stockTrade
     * @return
     */
    @PostMapping
    public ResponseEntity<StockTrade> save(@RequestBody StockTrade stockTrade) {
        return new ResponseEntity<StockTrade>(stockService.save(stockTrade), HttpStatus.CREATED);
    }

    /**
     * Method for updating stock
     * @param stockTrade
     * @return
     */
    @PutMapping
    public ResponseEntity<StockTrade> update(@RequestBody StockTrade stockTrade) {
        try {
            return new ResponseEntity<StockTrade>(stockService.update(stockTrade), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + stockTrade + "]");
            return new ResponseEntity<StockTrade>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Method for deleting stock
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        try {
            stockService.delete(id);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch(EmptyResultDataAccessException ex) {
            LOG.debug("delete for unknown id: [" + id + "]");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }


}
