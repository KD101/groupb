package com.training.springhackathon.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.training.springhackathon.model.StockTrade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.training.springhackathon.TestUtils.toJsonString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class StockControllerRequestTest {

    private final String REQUEST_PATH = "/api/v1/stock";
    @Autowired
    private MockMvc mvc;

    @Test
    void testGetValidResponse() throws Exception {
        mvc.perform(get(REQUEST_PATH))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void testGetInvalidResponse() throws Exception {
        mvc.perform(get("/not/valid"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void testPostRequest() throws Exception {
        // Example URL: http://localhost:8080/api/v1/stock?buyOrSell=buy&price=42.00&statusCode=1&stockTicker=Mr%20Mann&volume=12
        StockTrade trade = new StockTrade(2L, "John Doe II", 66.66, 69, "buy", 10);

        mvc.perform(post(REQUEST_PATH)
                .content(toJsonString(trade))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void testPutRequest() throws Exception {
        StockTrade updatedTrade = insertDummyTrade();
        updatedTrade.setStockTicker("Stanley Kubrick");
        mvc.perform(put(REQUEST_PATH)
                .content(toJsonString(updatedTrade))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteRequest() throws Exception {
        insertDummyTrade();
        mvc.perform(delete(REQUEST_PATH + "/{id}", 1L))
                .andExpect(status().isAccepted());
    }

    public StockTrade insertDummyTrade() throws Exception {
        StockTrade trade = new StockTrade(1L, "John Doe", 66.66, 69, "buy", 10);

        MvcResult mvcResult = mvc.perform(post(REQUEST_PATH)
                .content(toJsonString(trade))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        return new ObjectMapper().readValue(
                mvcResult.getResponse().getContentAsString(),
                new TypeReference<>() {});

    }





}
