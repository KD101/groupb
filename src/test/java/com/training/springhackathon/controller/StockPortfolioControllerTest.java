package com.training.springhackathon.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.springhackathon.model.StockPortfolio;
import com.training.springhackathon.model.StockTrade;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.training.springhackathon.TestUtils.toJsonString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class StockPortfolioControllerTest {

    private final String REQUEST_PATH = "/api/v1/portfolio";
    @Autowired
    private MockMvc mvc;

    @BeforeEach
    void setUp() { }

    @AfterEach
    void tearDown() { }

    @Test
    void testValidGetResponse() throws Exception {
        mvc.perform(get(REQUEST_PATH))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void testInvalidGetResponse() throws Exception {
        mvc.perform(get("/not/valid"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    void testPostRequest() throws Exception {
        StockPortfolio portfolio = new StockPortfolio("BT");

        mvc.perform(post(REQUEST_PATH)
                .content(toJsonString(portfolio))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void testPutRequest() throws Exception {
        StockPortfolio updatedPortfolio = insertDummyPortfolio();
        updatedPortfolio.setHoldings(54);
        mvc.perform(put(REQUEST_PATH)
                .content(toJsonString(updatedPortfolio))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteRequest() throws Exception {
        StockPortfolio portfolio = insertDummyPortfolio();
        mvc.perform(delete(REQUEST_PATH + "/{id}", portfolio.getId()))
                .andExpect(status().isAccepted());
    }

    public StockPortfolio insertDummyPortfolio() throws Exception {
        StockPortfolio portfolio = new StockPortfolio("BLK");

        MvcResult result = mvc.perform(post(REQUEST_PATH)
                .content(toJsonString(portfolio))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        return new ObjectMapper().readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<>() {});
    }
}