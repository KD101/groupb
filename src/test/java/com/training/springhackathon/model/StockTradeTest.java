package com.training.springhackathon.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StockTradeTest {

    private StockTrade trade;

    @BeforeEach
    void setUp() {
        trade = new StockTrade();
    }

    @Test
    void setId() {
        long id = 42;
        trade.setId(id);
        assertEquals(id, trade.getId());
    }

    @Test
    void setStockTicker() {
        String ticker = "Elon Tusk";
        trade.setStockTicker(ticker);
        assertEquals(ticker, trade.getStockTicker());
    }

    @Test
    void setPrice() {
        double price = 34.42;
        trade.setPrice(price);
        assertEquals(price, trade.getPrice());
    }

    @Test
    void setVolume() {
        int volume = 69;
        trade.setVolume(volume);
        assertEquals(volume, trade.getVolume());
    }

    @Test
    void setBuyOrSell() {
        String side = "Buy";
        trade.setBuyOrSell(side);
        assertEquals(side, trade.getBuyOrSell());
    }

    @Test
    void setStatusCode() {
        int status = 10;
        trade.setStatusCode(status);
        assertEquals(status, trade.getStatusCode());
    }
}