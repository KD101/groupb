package com.training.springhackathon.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StockPortfolioTest {

    private StockPortfolio portfolio;

    @BeforeEach
    void setUp() {
        portfolio = new StockPortfolio();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void setId() {
        long id = 69;
        portfolio.setId(id);
        assertEquals(id, portfolio.getId());
    }

    @Test
    void setStockTicker() {
        String ticker = "BLK";
        portfolio.setStockTicker(ticker);
        assertEquals(ticker, portfolio.getStockTicker());
    }

    @Test
    void setHoldings() {
        int holdings = 42;
        portfolio.setHoldings(holdings);
        assertEquals(holdings, portfolio.getHoldings());
    }

    @Test
    void setAvgPrice() {
        double avgPrice = 42.69;
        portfolio.setAvgPrice(avgPrice);
        assertEquals(avgPrice, portfolio.getAvgPrice());
    }

    @Test
    void setValue() {
        double value = 69.42;
        portfolio.setValue(value);
        assertEquals(value, portfolio.getValue());
    }
}