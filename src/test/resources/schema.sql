CREATE TABLE stocktrades
(tradeid INT AUTO_INCREMENT,
 stockticker VARCHAR(20),
 price DOUBLE,
 volume INT,
 buyOrSell VARCHAR(10),
 statusCode INT,
 date VARCHAR(30),
 PRIMARY KEY(tradeid)
);

CREATE TABLE stockportfolio
(portfolioid INT AUTO_INCREMENT,
 stockticker VARCHAR(255) NOT NULL,
 avgprice DOUBLE PRECISION,
 holdings INTEGER,
 value DOUBLE PRECISION,
 PRIMARY KEY(portfolioid)
);

INSERT INTO stocktrades (stockTicker, price, volume, buyOrSell, statusCode)  VALUES('BTC', '60000', 1, 'buy', 400);


